function getValueFromForm() {
  let _id = document.getElementById('idUser').value;
  let _user = document.getElementById('account').value;
  let _name = document.getElementById('name').value;
  let _email = document.getElementById('email').value;
  let _password = document.getElementById('password').value;
  let _image = document.getElementById('image').value;
  let _type = document.getElementById('typeUser').value;
  let _language = document.getElementById('language').value;
  let _desc = document.getElementById('desc').value;

  return new User(_id, _user, _name, _password, _email, _image, _language, _type, _desc);
}

function reset() {
  document.getElementById('account').value = '';
  document.getElementById('name').value = '';
  document.getElementById('email').value = '';
  document.getElementById('password').value = '';
  document.getElementById('image').value = '';
  document.getElementById('typeUser').value = 'Chọn loại người dùng';
  document.getElementById('language').value = 'Chọn ngôn ngữ';
  document.getElementById('desc').value = '';
}

function searchPositionEl(id, arr) {
  let position = -1;

  for (let index = 0; index < arr.length; index++) {
    let staff = arr[index];

    if (staff.account == id) {
      position = index;
      break;
    }
  }
  return position;
}

function showInformationUser(user) {
  document.getElementById('idUser').value = user.id;
  document.getElementById('account').value = user.user;
  document.getElementById('name').value = user.name;
  document.getElementById('email').value = user.email;
  document.getElementById('password').value = user.password;
  document.getElementById('image').value = user.avatar;
  document.getElementById('typeUser').value = user.type ? 'GV' : 'HS';
  document.getElementById('desc').value = user.desc;
  document.getElementById('language').value = user.language;
}

function onLoading() {
  document.getElementById('spinner').style.display = 'flex';
}

function offLoading() {
  document.getElementById('spinner').style.display = 'none';
}
