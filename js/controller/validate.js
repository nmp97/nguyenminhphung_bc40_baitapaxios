function checkDuplicate(user, arr, idErr) {
  var index = arr.findIndex(function (item) {
    return user == item.user;
  });

  if (index == -1) {
    document.getElementById(idErr).innerText = '';
    return true;
  } else {
    document.getElementById(idErr).innerText = 'Tài khoản đã tồn tại';
    return false;
  }
}

function checkLength(value, min, max, idErr) {
  let length = value.length;

  if (length < min || length > max) {
    document.getElementById(idErr).innerText = `Vui lòng nhập từ ${min} đến ${max} kí tự`;
    return false;
  } else {
    document.getElementById(idErr).innerText = '';
    return true;
  }
}

function checkNumber(value, idErr) {
  let reg = /^\d+$/;
  let isNumber = reg.test(value);
  if (isNumber) {
    document.getElementById(idErr).innerText = '';
    return true;
  } else {
    document.getElementById(idErr).innerText = 'Vui lòng nhập số';
    return false;
  }
}

function checkString(value, idErr) {
  let reg = /\d/;
  let isString = reg.test(value);
  if (isString || value == '') {
    document.getElementById(idErr).innerText = 'Vui lòng nhập chữ';
    return false;
  } else {
    document.getElementById(idErr).innerText = '';
    return true;
  }
}

function checkEmail(value, idErr) {
  let reg =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  let isEmail = reg.test(value);
  if (isEmail == false || value == '') {
    document.getElementById(idErr).innerText = 'Vui lòng nhập email hợp lệ';
    return false;
  } else {
    document.getElementById(idErr).innerText = '';
    return true;
  }
}

function checkPassword(value, idErr) {
  let reg = /^(?=.*\d)(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{6,8}$/;

  let isPassword = reg.test(value);
  if (isPassword == false || value == '') {
    document.getElementById(idErr).innerText =
      'Vui lòng nhập 6-8 kí tự có ít nhất một chữ số, một chữ cái in hoa và một ký tự đặc biệt ';
    return false;
  } else {
    document.getElementById(idErr).innerText = '';
    return true;
  }
}

function checkEmpty(value, idErr) {
  if (value.length == 0) {
    document.getElementById(idErr).innerText = 'Trường này không được để trống';
    return false;
  } else {
    document.getElementById(idErr).innerText = '';
    return true;
  }
}

function checkSpecialCharacters(value, idErr) {
  let regex = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
  let isSpecialChar = regex.test(value);

  if (isSpecialChar) {
    document.getElementById(idErr).innerText = 'Trường này không có ký tự đặc biệt';
    return false;
  } else {
    document.getElementById(idErr).innerText = '';
    return true;
  }
}

function checkURL(value, idErr) {
  let regex =
    /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/;

  let isURL = regex.test(value);

  if (isURL) {
    document.getElementById(idErr).innerText = '';
    return true;
  } else {
    document.getElementById(idErr).innerText = 'Đường dẫn không hợp lệ';
    return true;
  }
}

function checkType(value, idErr) {
  if (value == 'Chọn loại người dùng') {
    document.getElementById(idErr).innerHTML = 'Vui lòng chọn loại người dùng';
    return false;
  } else {
    document.getElementById(idErr).innerHTML = '';
    return true;
  }
}

function checkLanguage(value, idErr) {
  if (value == 'Chọn ngôn ngữ') {
    document.getElementById(idErr).innerHTML = 'Vui lòng chọn ngôn ngữ';
    return false;
  } else {
    document.getElementById(idErr).innerHTML = '';
    return true;
  }
}
