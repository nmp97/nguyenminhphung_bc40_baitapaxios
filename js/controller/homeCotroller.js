window.addEventListener('scroll', (event) => {
  let scroll = this.scrollY;
  if (scroll > 300) {
    document.querySelector('header').style.background = 'white';
    document.querySelector('header').style.boxShadow = 'rgba(0, 0, 0, 0.15) 0px 3px 3px 0px';
  } else {
    document.querySelector('header').style.background = 'transparent';
    document.querySelector('header').style.boxShadow = 'none';
  }
});
