const BASE_URL = 'https://63b2c99a5901da0ab36dba15.mockapi.io';

function renderUser(users) {
  let contentHTML = '';
  let stt = 0;

  users.forEach(function (user) {
    stt++;
    let content = `
                      <tr>
                        <td class="border border-slate-600">${stt}</td>
                        <td class="border border-slate-600">${user.user}</td>
                        <td class="border border-slate-600">${user.password}</td>
                        <td class="border border-slate-600">${user.name}</td>
                        <td class="border border-slate-600">${user.email}</td>
                        <td class="border border-slate-600">${user.language}</td>
                        <td class="border border-slate-600">${user.type}</td>
                        <td class="border border-slate-600">
                            <span class="edit mr-2" data-bs-toggle="modal" data-bs-target="#modal" onclick='editUser("${user.id}")')>Sửa</span>
                            <span class="delete" onclick='deleteUser("${user.id}")'>Xóa</span>
                        </td>
                      </tr>
        `;

    contentHTML += content;
  });
  document.getElementById('tableUser').innerHTML = contentHTML;
}

function fetchUsers() {
  onLoading();
  axios({
    url: `${BASE_URL}/user`,
    method: 'GET',
  })
    .then(function (res) {
      offLoading();
      renderUser(res.data);
    })
    .catch(function (err) {
      offLoading();
      console.log(err.message);
    });
}

fetchUsers();
