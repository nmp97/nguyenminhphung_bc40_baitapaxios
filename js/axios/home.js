const BASE_URL = 'https://63b2c99a5901da0ab36dba15.mockapi.io';

function renderHome(items) {
  contentHTML = '';

  items.reverse().forEach(function (item) {
    if (item.type == 'GV') {
      let content = `
                        <div class="user__item">
                            <div class="item__img">
                                <img src='${item.avatar}' alt="" />
                            </div>
                            <div class="item__desc text-center px-6 pt-4 pb-8">
                                <span>${item.language}</span>
                                <h3 class='my-3'>${item.name}</h3>
                                <p>${item.desc}</p>
                            </div>
                        </div>
            `;
      contentHTML += content;
    }
  });
  document.getElementById('userItem').innerHTML = contentHTML;
}

function fetchHome() {
  onLoading();
  axios({
    url: `${BASE_URL}/user`,
    method: 'GET',
  })
    .then(function (res) {
      offLoading();
      renderHome(res.data);
    })
    .catch(function (err) {
      offLoading();
      console.log(err.message);
    });
}

fetchHome();

