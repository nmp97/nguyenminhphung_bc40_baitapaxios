function User(_id, _user, _name, _password, _email, _avatar, _language, _type, _desc) {
  this.id = _id;
  this.user = _user;
  this.name = _name;
  this.password = _password;
  this.email = _email;
  this.avatar = _avatar;
  this.language = _language;
  this.type = _type;
  this.desc = _desc;
}
