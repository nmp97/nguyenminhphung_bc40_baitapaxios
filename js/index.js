//------------- Start Admin Page ----------------//

// Add user
document.getElementById('removeBtn').onclick = () => {
  document.getElementById('account').disabled = false;
  console.log('object');
};
function addUser() {
  let user = getValueFromForm();
  axios({
    url: `${BASE_URL}/user`,
    method: 'GET',
  })
    .then(function (res) {
      let isAccount =
        checkEmpty(user.user, 'accountNotice') &&
        checkDuplicate(user.user, res.data, 'accountNotice');
      let isName =
        checkEmpty(user.name, 'nameNotice') &&
        checkString(user.name, 'nameNotice') &&
        checkSpecialCharacters(user.name, 'nameNotice');
      let isEmail = checkEmpty(user.email, 'emailNotice') && checkEmail(user.email, 'emailNotice');
      let isPassword =
        checkEmpty(user.password, 'passwordNotice') &&
        checkPassword(user.password, 'passwordNotice');
      let isURL = checkEmpty(user.avatar, 'urlNotice') && checkURL(user.avatar, 'urlNotice');
      let isType = checkType(user.type, 'typeNotice');
      let isLanguage = checkLanguage(user.language, 'languageNotice');
      let isDesc =
        checkEmpty(user.desc, 'descNotice') && checkLength(user.desc, 1, 60, 'descNotice');

      let isValid =
        isAccount & isName & isEmail & isPassword & isURL & isType & isLanguage & isDesc;

      if (isValid) {
        axios({
          url: `${BASE_URL}/user`,
          method: 'POST',
          data: user,
        })
          .then(function (res) {
            fetchUsers();
          })
          .catch(function (err) {
            console.log(err);
          });
      }
    })
    .catch(function (err) {
      console.log(err);
    });
}

// Delete user
function deleteUser(id) {
  onLoading();
  axios({
    url: `${BASE_URL}/user/${id}`,
    method: 'DELETE',
  })
    .then(function (res) {
      offLoading();
      fetchUsers();
    })
    .catch(function (err) {
      offLoading();
      console.log(err);
    });
}

// Edit user
function editUser(id) {
  document.getElementById('account').disabled = true;
  axios({
    url: `${BASE_URL}/user/${id}`,
    method: 'GET',
  })
    .then(function (res) {
      showInformationUser(res.data);
    })
    .catch(function (err) {
      console.log(err);
    });
}

// Update user
function updateUser() {
  var user = getValueFromForm();
  onLoading();
  // Validate
  offLoading();
  let isName =
    checkEmpty(user.name, 'nameNotice') &&
    checkString(user.name, 'nameNotice') &&
    checkSpecialCharacters(user.name, 'nameNotice');

  let isEmail = checkEmpty(user.email, 'emailNotice') && checkEmail(user.email, 'emailNotice');
  let isPassword =
    checkEmpty(user.password, 'passwordNotice') && checkPassword(user.password, 'passwordNotice');
  let isURL = checkEmpty(user.avatar, 'urlNotice') && checkURL(user.avatar, 'urlNotice');
  let isType = checkType(user.type, 'typeNotice');
  let isLanguage = checkLanguage(user.language, 'languageNotice');
  let isDesc = checkEmpty(user.desc, 'descNotice') && checkLength(user.desc, 1, 60, 'descNotice');

  let isValid = isName & isEmail & isPassword & isURL & isType & isLanguage & isDesc;

  if (isValid) {
    axios({
      url: `${BASE_URL}/user/${user.id}`,
      method: 'PUT',
      data: user,
    })
      .then(function (res) {
        offLoading();
        fetchUsers();
      })
      .catch(function (err) {
        offLoading();
        console.log(err);
      });
  }
}
